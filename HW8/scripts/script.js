// ЗАДАНИЕ 1

const activationButton = document.getElementById('button');

// Генератор случайного цвета.
const createColor = () => {
    const color = ['#'];
    const colorValues = ['a', 'b', 'c', 'd', 'e', 'f', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    for(let i = 0; i < 6; i++) {
        color.push(colorValues[Math.floor(Math.random() * colorValues.length)])
    };
    return color.join('');
};


// Добавление 100 кругов на страницу
// Все круги добавляю в div #wrapper, чтобы потом на нем вызывать онклик для удаления выбранного круга
activationButton.addEventListener('click', () => {
    for (let i = 1; i <= 100; i++) {
        const circle = document.createElement('div');
        document.getElementById('wrapper').append(circle);
        circle.setAttribute('style', 'border: 1px solid black; width: 10px; height: 10px; border-radius: 50%; display: inline-block; background-color:' + createColor() + '; margin: 2px');
    };
});

// Удаление кругов по клику
document.getElementById('wrapper').addEventListener('click', (e) => {
    // e.target.setAttribute('style', 'display: none'); 
    e.target.style.display = 'none';
})