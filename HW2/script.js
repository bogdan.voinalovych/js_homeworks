// ЗАДАНИЕ 1

//a + b < 4 ? document.write('Мало') : document.write('Много');
// Если a + b < 4 = true, в браузере выведется строка "Мало", если false, переменной выведется "Много".


// ЗАДАНИЕ 2 

// var message;
// var login = prompt('Введите логин:'); // значение переменной login задается пользователем
// login == 'Вася' ? message = 'Привет!' : message = ''; // Если введенный пользователем login Вася, переменной message присваивается значение 'Привет!'. Если введенный пользователем login не 'Вася', у переменной message значение остается undefined.
// login == 'Директор' ? message = 'Здравствуйте!' : ''; // Если введенный пользователем login Директор, переменной message присваивается значение 'Здравствуйте!'. Если введенный пользователем login не 'Директор', у переменной message значение остается undefined.
// login == '' ? message = 'Нет логина' : ''; // Если пользователь не ввел ничего и нажал Enter, переменной message присваивается значение 'Нет логина'.
// alert(message) // Выводит на экран сообщение, равное переменной message, значение которой зависит от введенного пользователем логина.
// Если пользователь ввел логин, который не соответствует ни 'Вася', ни 'Директор', ни '', на экран выведется пустой alert.


// ЗАДАНИЕ 3

// var a = 20, b = 50;

// Вывод на экран суммы чисел, расположенных в промежутке от 20 до 50.

// var sum = 0; // Создаю переменную sum, в которой высчитается сумма чисел в диапазоне от 20 до 30.
// while (a <= b) { // Устанавливаю условие, при котором циклическая функция while будет производить итерацию до тех пор, пока a < b.
//     sum += a; // Пока соблюдается заданное условие, переменной sum задается значение, которое изначально является суммой 0 и 20.
//     a++; // После каждой итерации значение переменной a увеличивается на единицу, пока выполняется условие.
// }
// console.log(sum); // Вывожу в консоль сумму чисел в диапазоне от 20 до 50.

// Вывод на экран нечетных значений, расположенных в промежутке между 20 и 50.

// for (let i = 20; i <= 50; i++) { // После кажддой итерации значение i увеличивается на 1.
//     if (i % 2 == !0) // Устанавливаю условие, что остаток от деления i на 2 не должен равняться 0. 
//     document.write(i + '<br>') // На странице выводится каждое нечетное число в диапазоне от 20 до 50 + перенос строки.
// }


// ЗАДАНИЕ 4

// Прямоугольник

// for (let i = 0; i < 20; i++) {
//     for (let j = 0; j < 100; j++) {
//         document.write('*')
//     }
//     document.write('<br>')
// }

// Прямоугольный треугольник
// var b = 1;
// for (let i = 0; i < 20; i++) {
//     for (let j = 0; j < b; j++) {
//         document.write('*');
//     }
//     b++;
//     document.write('<br>')
// }

// Равносторонний треугольник

// var space = 19;
// var star = 1;
// var line = 20;
// for (var i = 0; i <= line; i++){
//     for (var j = 0; j <= space; j++){
//         document.write("&nbsp")
//     }
//     for (var k = 0; k < star; k++){
//         document.write("*")
//     }
//     space--;
//     star++;
//     document.write("<br>");
// }

// Ромб

// var lines = 42, spaces = 30, stars = 1;
// for (let i = 0; i <= lines; i++) {
//     for (let j = 0; j <= spaces; j++) {
//         document.write('&nbsp');
//     };
//     for (let k = 0; k < stars; k++) {
//         document.write('*');
//     };
//     spaces--;
//     stars++;
//     if (i >= lines / 2 && i <= lines) {
//         stars = stars - 2;
//         spaces = spaces + 2;
//     };
//     document.write('<br>');
// }