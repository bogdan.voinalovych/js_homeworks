// ЗАДАНИЕ 1

// // Создаю конструктор Human, в котором будут свойства name и age.
// class Human {
//     constructor (name, age) {
//         this.name = name,
//         this.age = age;
//     }
// }; // Сначала конструктор создавал стандартно, по типу 
// // "const Human = function (name, age) {this.name = name, this.age = age}".
// // Но VSCode предложил использовать вариант, введенный ES6. Я согласился:)

// // Создаю 4 новых объекта по конструктору
// const vanya = new Human ('Ваня', 27);
// const dima = new Human ('Дима', 20);
// const ira = new Human ('Ира', 19);
// const tanya = new Human ('Таня', 32);

// // создаю массив, который в дальнейшем буду сортировать
// const humans = [vanya, dima, ira, tanya];

// // Вызываю метод sort(), который принимает анонимную функцию, которая в себя принимает два аургмента - a и b.
// // Дальше функция sort() работает следующим образом (объясняю, как я это для себя понял после гугла и ютуба):
// // Сначала в качестве "а" подставляется первый элемент массива (индекс 0), а в качестве "b" - следующий элемент
// // (индекс 1). Они сравниваются, и функция возвращает 1, если элемент с 0 индексом больше элемента с
// // 1 индексом. Тогда происходит перестановка сравниваемых элементов. Если же элемент с 0 индексом меньше
// // элемента с 1 индексом, перестановки сравниваемых элементов массива не происходит. Потом сравниваются 
// // элемент с 1 индексом и элемент со 2 индексом, и так далее по порядку.
// // Первый проход по массиву заканчивается тем, что самое большое число массива выводится в самый конец массива.
// // То же самое происходит и при следующих проходах массива, пока, как я понял, завершится проход, при котором
// // не произойдет ни одной перестановки.
// // В данной функции я сравнивал свойства age каждого объекта, сортируя элементы массива humans по возрастанию.

// humans.sort(function (a, b) {
//     if (a.age > b.age) {
//         return 1;
//     }
//     if (a.age < b.age) {
//         return -1;
//     }
//       return 0;
//     });

// // Потом в нагуглил еще, что можно вместо всех этих if-ов написать просто "return a - b". В таком случае 
// // если возвращается положительное число, перестановка происходит, если отрицательное или 0 - 
// // перестановки не происходит.

// // Вывожу в консоль отсортированный по возрастанию значения age массив humans.
// console.log(humans);

// // Создаю функцию, которая будет сортировать элементы массива humans по убыванию свойства age каждого из элементов.
// // Фактически, просто меняю return на противоположный тому, что был в предыдущей функции внутри sort.
// humans.sort(function (a, b) {
//     if (a.age > b.age) {
//         return -1;
//     }
//     if (a.age < b.age) {
//         return 1;
//     }
//       return 0;
//     });

// // Вывожу в консоль отсортированный по убыванию значения age массив humans.
// console.log(humans);

// ================================================================================================================

// ЗАДАНИЕ 2

class Human {
    constructor(name, age, job, budget) {
        this.name = name,
            this.age = age,
            this.job = job,
            this.budget = budget
    }
    static totalBudget(array) {                    // Статическая функция, которая возвращает сумму бюджетов всех экземпляров класса Human.         
        let summ = 0;
        for (let i = 0; i < array.length; i++) {
            summ += array[i].budget
        };
        return summ
    };
    static averageAge(array) {                     // Статическая функция, которая возвращает среднее арифметическое возраста всех экземпляров класса Human
        let summ = 0;
        for (let i = 0; i < array.length; i++) {
            summ += array[i].age;
        };
        return parseInt(summ / array.length)       // Без parseInt может возвратить нецелое число. Использовал parseInt, чтобы отбросить нецелую часть. Так же можно использовать Math.float или Math.round (округлит до ближайшего целого).
    }
};

const humans = [                                  // Массив, чтобы работать с ним в static методах класса Human.
    new Human('Oleg', 42, 'Gambler', 10000),
    new Human('Igor', 25, 'Developer', 50000),
    new Human('Masha', 30, 'Waiter', 40000),
    new Human('Ira', 22, 'Manager', 25000)
];

console.log(Human.totalBudget(humans));           // 125000
console.log(Human.averageAge(humans));            // 29