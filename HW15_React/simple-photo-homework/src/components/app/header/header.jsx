import React from 'react';
import HeaderListItems from './header-list/header-list-items/header-list-items';
import HeaderList from './header-list/header-list';

const Header = () => {
    return (
        <>
            <HeaderList>
                <HeaderListItems />
            </HeaderList>
        </>
    )
};

export default Header;