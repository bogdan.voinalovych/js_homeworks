import React from 'react';
import HeaderListItems from './header-list-items/header-list-items';
import './header-list.css'

const HeaderList = () => {
    return (
        <>
            <ul>
                <HeaderListItems />
            </ul>
        </>
    )
};

export default HeaderList;