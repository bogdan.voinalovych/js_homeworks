import React from 'react';
import './header-list-items.css'

const HeaderListItems = () => {
    return (
        <>
            <li><a href="#">Home</a></li>
            <li><a href="#">Photoapp</a></li>
            <li><a href="#">Design</a></li>
            <li><a href="#">Download</a></li>
        </>
    )
};

export default HeaderListItems;