import React from 'react';
import Header from './header/header';
import HeaderList from './header/header-list/header-list';
import HeaderListItems from './header/header-list/header-list-items/header-list-items';
import ImageBlock from './image_block/image-block';
import TextBlock from './text-block/text-block';
import Footer from './footer/footer'

const App = () => {
    return (
        <div>
            <Header>
                <HeaderList>
                    <HeaderListItems />
                </HeaderList>
            </Header>
            <ImageBlock />
            <TextBlock />
            <Footer />
        </div>
    )
};

export default App;