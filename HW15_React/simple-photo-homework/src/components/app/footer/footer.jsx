import React from 'react';
import './footer.css'

const Footer = () => {
    return (
        <div className='footer'>Copyright by phototime - all right reserved</div>
    )
}

export default Footer;