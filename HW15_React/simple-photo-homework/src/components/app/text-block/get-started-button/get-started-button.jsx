import React from "react";
import "./get-started-button.css";

const GetStartedButton = () => {
  return (
      <input type="button" value="Get started" />
  );
};

export default GetStartedButton;
