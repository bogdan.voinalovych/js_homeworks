const Document = {
    heading: 'Креативный заголовок',
    body: 'Интересный и умный текст',
    footer: 'Какая-то информация в футере',
    date: '22.09.2020',
    appendix: {
        heading: 'Еще один креативный заголовок',
        body: 'Невероятный контент',
    }
}

Document.show = function() {
    document.write(`Заголовок документа - "${Document.heading}",<br> тело документа - "${Document.body}",<br> 
        а футер состоит их надписи "${Document.footer}". <br> 
        При этом всем дата составления этого невероятного документа - "${Document.date}".<hr>`)
    };

Document.appendix.show = function() {
    document.write(`Заголовок приложения - "${Document.appendix.heading}".<br>`);
    document.write(`Приложение главного документа - это просто ${Document.appendix.body}!!!<br>`);
    document.write(`В нижней части, конечно же, находится раздел "${Document.appendix.footer}".<br>`);
    document.write(`А это невероятное приложение к документу составлено ${Document.appendix.date}!`)
}

Document.appendix.footer = 'Контактная информация';
Document.appendix.date = '22.09.2020';

console.log(Document.show());

console.log(Document.appendix.show())