// ЗАДАНИЕ 1

const someArray = [1, 2, 3, 4, 50];

function double(array) {
    return array * 2;
}

function map(fn, array) {
    let a = [];
    for (let i = 0; i < array.length; i++){
        a.push(fn(array[i]));
    }
    return a
}

console.log(map(double, someArray));


// --------------------------------------------------------

// ЗАДАНИЕ 2 

// ----------------------------
// С оператором '?'

// function checkAge(age) {
//     return (age >= 18) ? true : confirm('Родители разрешили?');
// };
// console.log(checkAge(17));
// console.log(checkAge(22));

// ----------------------------
// С оператором '||'

// function checkAge(age){
//     return age > 18 || age == 18
// }
// console.log(checkAge(17));
// console.log(checkAge(22));

