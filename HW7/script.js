// ЗАДАНИЕ 1

// const words = ["программа", "макака", "прекрасный", "оладушек"];

// const word = words[Math.floor(Math.random() * words.length)];

// const answerArray = [];

// for (var i = 0; i < word.length; i++) {
//     answerArray[i] = "_";
// }

// let remainingLetters = word.length;

// while (remainingLetters > 0) {
//     alert(answerArray.join(" "));
//     let guess = prompt(`Угадайте букву, или нажмите Отмена для выхода из игры.`);
//     if (guess === null) {
//         break;
//     } else if (guess.length !== 1) {
//         alert("Пожалуйста, введите одиночную букву.");
//     } else {
//         for (var j = 0; j < word.length; j++) {
//             if (word[j] === guess) {
//                 answerArray[j] = guess;
//                 remainingLetters--;
//             }
//         }
//     }
// }

// alert(answerArray.join(" "));

// alert(`Отлично! Было загадано слово ${word}`);

// ===============================================================================

// ЗАДАНИЕ 2

// Реализовать функцию для создания объекта "пользователь".
// Написать функцию createNewUser(), которая будет создавать и возвращать
// объект newUser. При вызове функция должна спросить у вызывающего имя
// и фамилию. Используя данные, введенные пользователем, создать объект
// newUser со свойствами firstName и lastName. Добавить в объект newUser
// метод getLogin(), который будет возвращать первую букву имени
// пользователя, соединенную с фамилией пользователя, все в нижнем
// регистре (например, Ivan Kravchenko —› ikravchenko). Создать пользователя с
// помощью функции createNewUser(). Вызвать у пользователя функцию
// getLogin(). Вывести в консоль результат выполнения функции.

const newUser = {}

function createNewUser() {
    newUser.firstName = prompt('Введите Ваше имя:');
    newUser.lastName = prompt('Введите Вашу фамилию:');
    return newUser
}

createNewUser()

newUser.getLogin = () => { return (newUser.firstName[0] + newUser.lastName).toLowerCase() }

console.log(newUser.getLogin());

// ===============================================================================

// ЗАДАНИЕ 3

// Дополнить функцию createNewUserO методами подсчета возраста
// пользователя и его паролем.
// Возьмите выполненное задание выше (созданная вами функция
// createNewUser()) и дополните ее следующим функционалом: При вызове
// функция должна спросить у вызывающего дату рождения (текст B формате
// dd.mm.yyyy) и сохранить ее B поле birthday. Создать метод getAge() который
// будет возвращать сколько пользователю лет. Создать метод getPassword(),
// который будет возвращать первую букву имени пользователя в верхнем
// регистре, соединенную с фамилией (в нижнем регистре) и годом рождения.
// (например, Ivan Kravchenko 13.03.1992 —› lkravchenko1992). Вывести в
// консоль результат работы функции createNewUser(), a тaк жe функций
// getAge() и getPassword() созданного объекта.

createNewUser.birthDate = () => newUser.birthday = prompt('Введите дату рождения в формате дд.мм.гггг:');

createNewUser.birthDate();

const birthYear = newUser.birthday.substr(-4);

const currentDate = new Date;

newUser.getAge = () => currentDate.getFullYear() - birthYear;

const userYears = newUser.getAge();

newUser.getPassword = () => `${newUser.firstName[0].toUpperCase()}${newUser.lastName.toLowerCase()}${birthYear}`;

console.log(newUser.getAge());

console.log(newUser.getPassword());